/***************************************************************************
 * simple librt based runtime measurement class
 *
 * (c) 2008-2015 Kshitij Kulshreshtha
 *
 *  Permission is hereby granted, free of charge, to any person
 *  obtaining a copy of this software and associated documentation
 *  files (the "Software"), to deal in the Software without
 *  restriction, including without limitation the rights to use, copy,
 *  modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.

 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ***************************************************************************/
#ifndef KK_TIMER_HPP
#define KK_TIMER_HPP

#include <ostream>

using std::ostream;

class timehelper;

class timer {
protected:
   timespec ts;
   timehelper* th;
   int count;

public:
   timer();
   virtual ~timer();
   virtual void begin();
   virtual void end();
   virtual timer average() const;
   virtual double value() const;
   friend ostream& operator << (ostream&, const timer&);
};

ostream& operator << (ostream& out, const timer& t);

#endif
