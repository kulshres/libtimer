/***************************************************************************
 * simple librt based runtime measurement class
 *
 * (c) 2008-2015 Kshitij Kulshreshtha
 *
 *  Permission is hereby granted, free of charge, to any person
 *  obtaining a copy of this software and associated documentation
 *  files (the "Software"), to deal in the Software without
 *  restriction, including without limitation the rights to use, copy,
 *  modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the Software.

 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 ***************************************************************************/

#include "timer.hpp"
#include <cstring>
#include <iomanip>

using std::memset;
using std::setfill;
using std::setw;

static timespec operator - (const timespec& end, const timespec& start)
{
        timespec temp;
        if ((end.tv_nsec-start.tv_nsec)<0) {
                temp.tv_sec = end.tv_sec-start.tv_sec-1;
                temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
        } else {
                temp.tv_sec = end.tv_sec-start.tv_sec;
                temp.tv_nsec = end.tv_nsec-start.tv_nsec;
        }
        return temp;
}


static timespec& operator += (timespec& accum, timespec val)
{
   accum.tv_sec += val.tv_sec;
   accum.tv_nsec += val.tv_nsec;
   if ( accum.tv_nsec >= 1000000000 ) {
      accum.tv_sec += 1;
      accum.tv_nsec -= 1000000000;
   }
}


static timespec operator / (const timespec& val, int num)
{

   timespec res;

   if (num == 0) {
      res.tv_sec = 0;
      res.tv_nsec = 0;
      return res;
   }

   long long nsec;
   nsec = (long long)val.tv_sec * 1000000000 / num;
   nsec += (long long)val.tv_nsec / num;

   res.tv_sec = nsec / 1000000000;
   res.tv_nsec = nsec % 1000000000;

   return res;
}


static ostream& operator << (ostream& out, const timespec& t)
{
   char c = out.fill();
   out << setw(5) << t.tv_sec << ".";
   out.fill('0');
   out << setw(9) << t.tv_nsec;
   out.fill(c);
   out << " sec";
   return out;
}


ostream& operator<< (ostream& out, const timer& t)
{
   out << t.ts;
}

class timehelper {
public:
   timespec start, end;
   timehelper() {}
   virtual ~timehelper() {}
};

timer::timer() {
   th = NULL;
   count = 0;
   memset(&ts,0,sizeof(timespec));
}

timer::~timer() {
   if (th != NULL)
      delete th;
   th = NULL;
}

void timer::begin() {
   th = new timehelper;
   clock_gettime(CLOCK_MONOTONIC, &th->start);
}

void timer::end() {
   if (th != NULL) {
      clock_gettime(CLOCK_MONOTONIC, &th->end);
      ts += th->end - th->start;
      count++;
      delete th;
      th = NULL;
   } else {
      throw ("timer::end() called before timer::start()");
   }
}

timer timer::average() const {
   timer avg;
   if ( count >= 1 ) {
      avg.ts = ts / count;
      avg.count = 1;
      return avg;
   } else {
      return avg;
   }
}

double timer::value() const {
  if ( count >= 1 ) {
    double val = ts.tv_sec;
    val += 1.0e-9*ts.tv_nsec;
    return val;
  } else {
    return 0.0;
  }
}

